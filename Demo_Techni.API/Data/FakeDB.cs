﻿using Demo_Techni.API.Data.Entities;

namespace Demo_Techni.API.Data
{
    public class FakeDB
    {
        public List<Teacher> Teachers { get; set; }

        public FakeDB()
        {
            Teachers = new List<Teacher>()
            {
                new Teacher(){ TeacherId = 1, FirstName = "Aurélien", LastName = "Strimelle", NickName = "RegexMan", BirthDate = new DateTime(1989,11,1), HasPet = false, CreatedAt = DateTime.Now},
                new Teacher(){ TeacherId = 2, FirstName = "Aude", LastName = "Beurive", NickName = "Lhel", BirthDate = new DateTime(1989,10,16), HasPet = false, CreatedAt = DateTime.Now},
                new Teacher(){ TeacherId = 3, FirstName = "Gavin", LastName = "Chaineux", NickName = null, BirthDate = new DateTime(1993,10,18), HasPet = true, CreatedAt = DateTime.Now}
            };
        }
    }
}
