﻿namespace Demo_Techni.API.Data.Entities
{
    public class Teacher
    {
        public int TeacherId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string? NickName { get; set; }

        public DateTime BirthDate { get; set; }

        public bool HasPet { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
