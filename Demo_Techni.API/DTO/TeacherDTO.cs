﻿using System.ComponentModel.DataAnnotations;

namespace Demo_Techni.API.DTO
{
    //DTO Pour les données qu'on va afficher
    public class TeacherDTO
    {
        public int TeacherId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string? NickName { get; set; }

        public bool HasPet { get; set; }
    }

    //DTO Pour les données qu'on va recevoir du client
    public class TeacherDataDTO
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string? NickName { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        public bool HasPet { get; set; }

    }

}
