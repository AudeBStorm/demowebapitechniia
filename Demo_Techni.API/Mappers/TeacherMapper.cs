﻿using Demo_Techni.API.Data.Entities;
using Demo_Techni.API.DTO;

namespace Demo_Techni.API.Mappers
{
    public static class TeacherMapper
    {
        public static TeacherDTO ToDTO(this Teacher teacher)
        {
            return new TeacherDTO()
            {
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                HasPet = teacher.HasPet,
                NickName = teacher.NickName,
                TeacherId = teacher.TeacherId
            };
        }

        public static Teacher ToEntity(this TeacherDataDTO teacher)
        {
            return new Teacher()
            {
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                HasPet = teacher.HasPet,
                NickName = teacher.NickName,
                BirthDate = teacher.BirthDate
            };
        }
    }
}
