﻿using Demo_Techni.API.Data;
using Demo_Techni.API.Data.Entities;
using Demo_Techni.API.DTO;
using Demo_Techni.API.Mappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo_Techni.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly FakeDB _fakeDB;

        public TeacherController(FakeDB fakeDB)
        {
            //Récupération de notre FakeDB avec l'injection de dépendance
            _fakeDB = fakeDB;
        }

        //Méthodes CRUD 
        //C -> POST
        //R -> GET
        //U -> PUT/PATCH
        //D -> DELETE

        //GetAll (ou Get) : Action/Verb/Method Get pour récupérer toutes les entités
        [HttpGet]
        //Doc pour Swagger, pour indiquer le type de valeur retournée en cas de succès
        [ProducesResponseType(200, Type = typeof(IEnumerable<TeacherDTO>))]
        public IActionResult GetAll()
        {
            //On va devoir créer un DTO pour ne renvoyer que les infos qu'on souhaite afficher
            //TYPIQUEMENT jamais un mot de passe (même hash)
            IEnumerable<TeacherDTO> teachers = _fakeDB.Teachers.Select(t => t.ToDTO());
            //Equivalent puisque TeacherMapper est une classe static avec méthodes static qu'on peut utiliser directement
            //IEnumerable<TeacherDTO> teachers = _fakeDB.Teachers.Select(TeacherMapper.ToDTO);
            return Ok(teachers);
        }

        //GetById : Action Get pour récup UNE entité en particulier
        [HttpGet]
        [Route("{id}")]
        //Soit 
        //[HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(TeacherDTO))]
        [ProducesResponseType(404)]

        public IActionResult GetById([FromRoute]int id)
        {
            TeacherDTO? teacher = _fakeDB.Teachers.SingleOrDefault(t => t.TeacherId == id)?.ToDTO();
            if(teacher is null)
            {
                return NotFound();
            }
            return Ok(teacher);
        }

        //Create/Add/Insert (vous l'appelez comme vous voulez)
        //Action post : Pour ajouter des données
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(TeacherDTO))]
        //[ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public IActionResult Create([FromBody]TeacherDataDTO teacherToAdd)
        {
            Teacher teacher = teacherToAdd.ToEntity();
            teacher.CreatedAt = DateTime.Now;
            //On récupète le TeacherId le plus grand dans la "DB" et on lui ajoute un
            teacher.TeacherId = _fakeDB.Teachers.Max(t => t.TeacherId) + 1;
            _fakeDB.Teachers.Add(teacher);

            //Envoie un 204 mais c'est tout, n'envoie rien d'autre comme info
            //return NoContent();
            return CreatedAtAction(nameof(GetById), new { id = teacher.TeacherId }, teacher.ToDTO());
        }

        [HttpPut]
        [Route("{id}")]
        //Si renvoie de 204
        //[ProducesResponseType(204)]
        //si 201
        [ProducesResponseType(201, Type = typeof(TeacherDTO))]
        [ProducesResponseType(404)]

        public IActionResult Update([FromRoute]int id, [FromBody]TeacherDataDTO teacherToUpdate)
        {
            Teacher? teacher = _fakeDB.Teachers.SingleOrDefault(t => t.TeacherId == id);
            //Si pas trouvé en "DB"
            if(teacher is null)
            {
                return NotFound();
            }
            //Si trouvé en "DB", on peut le modifier
            teacher.FirstName = teacherToUpdate.FirstName;
            teacher.LastName = teacherToUpdate.LastName;
            teacher.BirthDate = teacherToUpdate.BirthDate;
            teacher.HasPet = teacherToUpdate.HasPet;
            teacher.NickName = teacherToUpdate.NickName;
            //Soit 204 : C'est ok mais on renvoie rien
            //return NoContent();
            //Soit 201 : C'est ok et en plus on t'envoie le teacher modifié + la route pour y accéder
            return CreatedAtAction(nameof(GetById), new { id = teacher.TeacherId }, teacher.ToDTO());
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult Delete([FromRoute]int id)
        {
            //On teste s'il existe un teacher avec cet id en "DB"
            if(!_fakeDB.Teachers.Exists(t => t.TeacherId == id))
            {
                return NotFound();
            }
            _fakeDB.Teachers.RemoveAll(t => t.TeacherId == id);
            return NoContent();
        }
        
    }
}
